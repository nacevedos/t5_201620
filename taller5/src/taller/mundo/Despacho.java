package taller.mundo;

public class Despacho implements Comparable<Despacho>
{

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Precio del pedido
	 */
	private double precio;
	
	/**
	 * Autor del pedido
	 */
	private String autorPedido;
	
	/**
	 * Cercania del pedido
	 */
	private int cercania;
	
	// ----------------------------------
	// Constructor
	// ----------------------------------
	
	/**
	 * Constructor del pedido
	 * TODO Defina el constructor de la clase
	 */
	public Despacho(double pPrecio,String pAutor,int pCercania){
		
		precio = pPrecio;
		autorPedido=pAutor;
		cercania = pCercania;
		
	}
	
	// ----------------------------------
	// Métodos
	// ----------------------------------
	
	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}
	
	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}
	
	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() {
		return cercania;
	}

	@Override
	public int compareTo(Despacho o) {
		if (this.cercania>o.cercania) {
			return 1;
		}
		else if (this.cercania<o.cercania) {
			return-1;
		}
		return 0;
	}
	
}
