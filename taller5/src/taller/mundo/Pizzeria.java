package taller.mundo;

import taller.estructuras.IHeap;
import taller.estructuras.MaxHeap;
import taller.estructuras.MinHeap;

public class Pizzeria 
{	
	// ----------------------------------
    // Constantes
    // ----------------------------------
	
	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";
	
	// ----------------------------------
    // Atributos
    // ----------------------------------
	
	/**
	 * Heap que almacena los pedidos recibidos
	 */
	// TODO 
	private MaxHeap <Pedido> pedidosRecibidos;
	/**
	 * Getter de pedidos recibidos
	 */
	// TODO 
	
 	/** 
	 * Heap de elementos por despachar
	 */
	// TODO 
	private MinHeap<Despacho> pedidosDespachar; 
	/**
	 * Getter de elementos por despachar
	 */
	public IHeap<Despacho> darDespachar()
	{
		return pedidosDespachar;
	}
	public IHeap<Pedido> darRecibidos()
	{
		return pedidosRecibidos;
	}
	// TODO 
	
	// ----------------------------------
    // Constructor
    // ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		pedidosRecibidos= new MaxHeap<Pedido>();
		
		
		pedidosDespachar= new MinHeap<Despacho>();
		
	}
		// TODO 
		
	
	// ----------------------------------
    // Métodos
    // ----------------------------------
	
	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		// TODO 
		Pedido nuevo= new Pedido(precio, nombreAutor, cercania); 
		pedidosRecibidos.add(nuevo);
		Despacho y = new Despacho(precio, nombreAutor, cercania);
		pedidosDespachar.add(y);

		
	}
	
	// Atender al pedido más importante de la cola
	
	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		// TODO 
		Pedido retornar=pedidosRecibidos.poll();
		
		return retornar;
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Despacho despacharPedido()
	{
		// TODO 
		Despacho retornar=pedidosDespachar.poll();
		
		return retornar;
	    
	}
	
	 /**
     * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
     * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
     */
     public Pedido [] pedidosRecibidosList()
     {
        // TODO 
        return pedidosRecibidos.darArreglo();
     }
     
      /**
       * Retorna la cola de prioridad de despachos como un arreglo. 
       * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
       */
     public Despacho [] colaDespachosList()
     {
         // TODO 
    	 
    	 return pedidosDespachar.darArreglo();
     }
}
