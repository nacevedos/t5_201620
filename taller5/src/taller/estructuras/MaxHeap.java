package taller.estructuras;

import java.util.*;

public class MaxHeap<T extends Comparable<T>> implements IHeap<T> {

	public final static int CAPACIDAD_INICIAL = 30;

	private int currentsize;

	private T[] heap;

	public MaxHeap() {

		currentsize = 0;
		heap = (T[]) new Comparable[CAPACIDAD_INICIAL];

	}

	public void add(T elemento) {

		// TODO Auto-generated method stub

		if (currentsize == heap.length-1  ) {

			doubleSize();

		}

		heap[++currentsize] = elemento;
		siftUp();

	}

	@Override
	public T peek() {
		// TODO Auto-generated method stub
		return heap[1];
	}

	@Override
	public T poll() {
		T max = heap[1];
		int ultimo = currentsize;
		
		exchange(1, ultimo);
		heap[ultimo] = null;
		currentsize -= 1;

		if ((currentsize > 0) && (currentsize == (heap.length - 1) / 4))
			resize(heap.length / 2);
		return max;
	}
	

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return currentsize;
	}

	@Override
	public boolean isEmpty() {
		if (currentsize == 0) {
			return true;
		}
		return false;
	}

	@Override
	public void siftUp() {
		int k = currentsize;
		while (k > 1 && menor(k / 2, k)) {

			exchange(k / 2, k);
			k = k / 2;
		}

		// TODO Auto-generated method stub

	}

	@Override
	public void siftDown() {

		int k = 1;
		while (k * 2 <= currentsize) {
			int j = 2 * k;
			if (j < currentsize && menor(j, j + 1)) {

				j++;
			}
			if (!menor(k, j)) {
				break;
			}
			exchange(k, j);
			k = j;
		}

	}

	private boolean menor(int i, int j) {
		// i menor que j
		if (heap[i]!=null)
		{
		return heap[i].compareTo(heap[j]) < 0;
		}
		else 
			return true;
		
	}

	private void exchange(int i, int j) {

		T t = heap[i];
		heap[i] = heap[j];
		heap[j] = t;

	}

	

	private void doubleSize() {
		T[] old = heap;
		currentsize = heap.length * 2;

		heap = (T[]) new Comparable[currentsize];
		System.arraycopy(old, 0, heap, 0, currentsize/2);
	}

	private void resize(int s) {

		T[] temp = (T[]) new Comparable[s];
		for (int i = 1; i <= currentsize; i++) {
			temp[i] = heap[i];
		}
		heap = temp;
	}
	public T[] darArreglo(){
		
		T[] resp = (T[]) new Comparable[50];
		MaxHeap<T> nuevo = new MaxHeap<T>();
		
		for (T object : heap ) 
		{
			nuevo.add(object);
		}

		int i=0;
		while(!nuevo.isEmpty())
		{
			T o = nuevo.poll();
			resp[i] = o;
			i++;
		}
		return resp;
		
	}

}
